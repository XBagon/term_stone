use std::io::{stdin,BufRead};
use crate::game::Game;
use strsim::levenshtein;

use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(subcommand)]
    command: Command
}

#[derive(StructOpt)]
enum Command {
    //#[strum(serialize="show",serialize="s")]
    #[structopt(name = "show")]
    Show(Show),
    //#[strum(serialize="play",serialize="p")]
    #[structopt(name = "play")]
    Play(Play),
}

#[derive(StructOpt)]
struct Show {
    #[structopt(raw(possible_values = "&Location::variants()", case_insensitive = "true"))]
    location: Location,
}

#[derive(StructOpt)]
struct Play {
    #[structopt(short = "c", long = "card")]
    card: String,
    #[structopt(short = "p", long = "position")]
    position: Option<usize>,
    #[structopt(short = "t", long = "target")]
    target_card: Option<String>,
}

arg_enum!{
    #[derive(StructOpt)]
    enum Location {
        Hand,
        Board,
    }
}

fn parse_card(game: &mut Game, player: usize, card: &str) -> Result<usize,()> {
    if let Ok(hand_position) = card.parse::<usize>() {
        Ok(game.environment.players[player].hand.cards[hand_position])
    } else {
        game.environment.card_iter(&game.environment.players[player].hand.cards).map(|(id,c)| (id,levenshtein(&c.name, card))).min().and_then(|min| Some(min.0)).ok_or(())
    }
}


pub fn start(game: &mut Game, player: usize) {
    for line in stdin().lock().lines() {
        let line = line.unwrap();
        let mut parts = std::iter::once("term_stone").chain(line.split_whitespace());
        let opt = match Opt::from_iter_safe(parts) {
            Ok(opt) => opt,
            Err(e) => {
                eprintln!("{}", e.message);
                continue;
            }
        };
        match opt.command {
            Command::Play(play) => {
                if let Ok(card) = parse_card(game, player, &play.card) {
                    let target = if let Some(target) = &play.target_card {
                        if let Ok(target) = parse_card(game, player, target) {
                            Some(target)
                        } else {
                            eprintln!("");
                            continue;
                        }
                    } else {
                        None
                    };
                    game.play_card(player, card, target, play.position);
                }
            },
            Command::Show(show) => {
                match show.location {
                    Location::Hand => {
                        for (index,(id,card)) in game.environment.card_iter(&game.environment.players[player].hand.cards).enumerate() {
                            print!("{{{}: {}}};    ", index, card);
                        }
                        println!();
                    },
                    Location::Board => {
                        for (id,player) in game.environment.players.iter().enumerate() {
                            print!("Player {}:", id);
                            for (index, (id, card)) in game.environment.card_iter(&player.board.cards).enumerate() {
                                print!("{{{}: {}}};    ", index, card);
                            }
                            println!();
                        }

                    },
                }
            },
        }
    }
}

//TODO: add event for requesting target and add method in interface for it.