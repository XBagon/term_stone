pub mod characters;
pub mod game_modes;
pub mod spells;
pub mod weapons;

use crate::{
    cards::{characters::Character, spells::Spell, weapons::Weapon},
    game::{events::*, Game},
};
use std::fmt;

pub trait ExtraData: std::fmt::Debug {
    fn box_clone(&self) -> Box<dyn ExtraData>;
}

impl ExtraData for () {
    fn box_clone(&self) -> Box<ExtraData> {
        Box::new(())
    }
}
#[derive(Derivative)]
#[derivative(Debug)]
pub struct Card {
    pub name: String,
    pub cost: i32,
    pub kind: CardKind,
    pub extra: Box<dyn ExtraData>,
    #[derivative(Debug = "ignore")]
    pub init: fn(&mut EventManager, usize),
}

#[derive(Clone, Debug)]
pub enum CardKind {
    Character(Character),
    Spell(Spell),
    Weapon(Weapon),
    GameMode,
}

impl fmt::Display for CardKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            CardKind::Character(character) => write!(f,"{}",character),
            CardKind::Spell(spell) => write!(f,"{}",spell),
            CardKind::Weapon(weapon) => write!(f,"{}",weapon),
            CardKind::GameMode => write!(f,"[GameMode]"),
        }
    }
}

impl Card {
    pub fn character(&mut self) -> Option<&mut Character> {
        if let Card {
            kind: CardKind::Character(character),
            ..
        } = self
        {
            Some(character)
        } else {
            None
        }
    }
}

impl Clone for Card {
    fn clone(&self) -> Self {
        Card {
            name: self.name.clone(),
            cost: self.cost.clone(),
            kind: self.kind.clone(),
            extra: self.extra.box_clone(),
            init: self.init.clone(),
        }
    }
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "({}) {} {}", self.cost, self.name, self.kind)
    }
}

#[derive(Constructor, Clone)]
pub struct GetManaCost {}

impl cost_pay::Action for GetManaCost {
    fn action(&self, game: &mut Game, card: usize, data: cost_pay::EventData) -> Option<cost_pay::EventData> {
        if card == data.paying_card {
            let hero_mana = game.environment.card(game.environment.players[data.player].hero).unwrap().character().unwrap().hero().unwrap().mana;
            game.trigger_mana_cost_check(mana_cost_check::EventData::new(data.player, card, 0)).and_then(|cost_data| {
                if hero_mana >= cost_data.cost {
                    Some(data)
                } else {
                    None
                }
            })
        } else {
            Some(data)
        }
    }
}

#[derive(Constructor, Clone)]
pub struct PayManaCost {}

impl cost_pay::Action for PayManaCost {
    fn action(&self, game: &mut Game, card: usize, data: cost_pay::EventData) -> Option<cost_pay::EventData> {
        if card == data.paying_card {
            game.trigger_mana_use(mana_use::EventData::new(data.player, card, data.cost));
        }
        Some(data)
    }
}

pub fn initialize_mana_cost(event_manager: &mut EventManager, card: usize) {
    event_manager.before_cost_pay.push(cost_pay::BeforeSubscription::new(card, Box::new(GetManaCost::new())));
    event_manager.on_cost_pay.push(cost_pay::OnSubscription::new(card, Box::new(PayManaCost::new())));
}
