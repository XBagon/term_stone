use std::fmt;

#[derive(Clone, Debug)]
pub struct Weapon {}

impl fmt::Display for Weapon {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "[Weapon]")
    }
}