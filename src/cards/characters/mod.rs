pub mod heroes;
pub mod minions;

use crate::cards::characters::heroes::Hero;
use std::fmt;

#[derive(Clone, Debug)]
pub struct Character {
    pub health: i32,
    pub max_health: i32,
    pub attack: i32,
    pub race: RaceFlags,
    pub kind: CharacterKind,
}

#[derive(Clone, Debug)]
pub enum CharacterKind {
    Hero(Hero),
    Minion,
}

impl fmt::Display for CharacterKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            CharacterKind::Hero(hero) => write!(f,"{}",hero),
            CharacterKind::Minion => write!(f,"[Minion]"),
        }
    }
}

impl Character {
    pub fn minion(&mut self) -> Option<&mut Character> {
        if let Character {
            kind: CharacterKind::Minion,
            ..
        } = self
            {
                Some(self)
            } else {
            None
        }
    }

    pub fn hero(&mut self) -> Option<&mut Hero> {
        if let Character {
            kind: CharacterKind::Hero(hero),
            ..
        } = self
            {
                Some(hero)
            } else {
            None
        }
    }
}

impl fmt::Display for Character {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "[Minion] A:{} H:{}/{} |{:?}|", self.attack, self.health, self.max_health, self.race)
    }
}

bitflags! {
    pub struct RaceFlags: u32 {
        const None = 0b1<<0;
        const Beast = 0b1<<1;
    }
}
