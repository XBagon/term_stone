use std::fmt;

pub mod jaina_proudmoore;

#[derive(Clone, Debug)]
pub struct Hero {
    pub mana: i32,
    pub max_mana: i32,
}

impl fmt::Display for Hero {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "[Hero] M:{}/{}", self.mana, self.max_mana)
    }
}