use crate::{
    cards::{
        characters::{heroes::Hero, Character, CharacterKind, RaceFlags},
        Card, CardKind,
    },
    game::events::EventManager,
};

pub fn new() -> Card {
    Card {
        name: String::from("Jaina Prodmoore"),
        cost: 1,
        kind: CardKind::Character(Character {
            health: 30,
            max_health: 30,
            attack: 0,
            race: RaceFlags::None,
            kind: CharacterKind::Hero(Hero {
                mana: 0,
                max_mana: 0
            }),
        }),
        extra: Box::new(()),
        init: initialize,
    }
}

fn initialize(_event_manager: &mut EventManager, _card: usize) {}
