use crate::{
    cards::{
        self,
        characters::{Character, CharacterKind, RaceFlags},
        Card, CardKind,
    },
    game::events::EventManager,
};

fn new() -> Card {
    Card {
        name: String::from("Dire Mole"),
        cost: 1,
        kind: CardKind::Character(Character {
            health: 3,
            max_health: 3,
            attack: 1,
            race: RaceFlags::Beast,
            kind: CharacterKind::Minion,
        }),
        extra: Box::new(()),
        init: initialize,
    }
}

fn initialize(event_manager: &mut EventManager, card: usize) {
    cards::initialize_mana_cost(event_manager, card);
}
