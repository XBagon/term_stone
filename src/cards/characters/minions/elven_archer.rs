use crate::{
    cards::{
        self,
        characters::{Character, CharacterKind, RaceFlags},
        Card, CardKind,
    },
    game::{
        events::{play, EventManager},
        Game,
    },
};

pub fn new() -> Card {
    Card {
        name: String::from("Elven Archer"),
        cost: 1,
        kind: CardKind::Character(Character {
            health: 1,
            max_health: 1,
            attack: 1,
            race: RaceFlags::None,
            kind: CharacterKind::Minion,
        }),
        extra: Box::new(()),
        init: initialize,
    }
}

fn initialize(event_manager: &mut EventManager, card: usize) {
    cards::initialize_mana_cost(event_manager, card);
    event_manager.on_play.push(play::OnSubscription::new(card, Box::new(Battlecry::new())));
}

#[derive(Constructor, Clone)]
pub struct Battlecry {}

impl play::Action for Battlecry {
    fn action(&self, game: &mut Game, card: usize, data: play::EventData) -> Option<play::EventData> {
        if card == data.playing_card {
            if let Some(target) = data.target {
                if let CardKind::Character(character) = &mut game.environment.cards[target].kind {
                    character.health -= 1; //TODO: trigger damage event instead
                }
            }
        }
        Some(data)
    }
}
