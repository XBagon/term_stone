use crate::{
    cards::{characters::CharacterKind, Card, CardKind},
    game::{events::*, Game},
};

pub fn new() -> Card {
    Card {
        name: String::from("Default Game Mode"),
        cost: 0,
        kind: CardKind::GameMode,
        extra: Box::new(()),
        init: initialize,
    }
}

fn initialize(event_manager: &mut EventManager, card: usize) {
    event_manager.on_game_start.push(game_start::OnSubscription::new(card, Box::new(InitialDraw::new())));
    event_manager.on_play.push(play::OnSubscription::new(card, Box::new(PlayCard::new())));
    event_manager.on_mana_use.push(mana_use::OnSubscription::new(card, Box::new(UseMana::new())));
}

#[derive(Constructor, Clone)]
struct PlayCard {}

impl play::Action for PlayCard {
    fn action(&self, game: &mut Game, _card: usize, data: play::EventData) -> Option<play::EventData> {
        game.trigger_cost_pay(cost_pay::EventData::new(data.player, data.playing_card, game.environment.cards[data.playing_card].cost)).and_then(
            |_cost_pay_data| {
                match &game.environment.card(data.playing_card).unwrap().kind {
                    CardKind::Character(character) => match character.kind {
                        CharacterKind::Hero(_) => unimplemented!(),
                        CharacterKind::Minion => {
                            if game.environment.players[data.player].board.cards.len() < 7 {
                                game.environment.players[data.player].play_at_position(data.playing_card, data.position);
                            }
                        }
                    },
                    _unimpl => unimplemented!("{:?}", _unimpl),
                }
                Some(data)
            },
        )
    }
}

#[derive(Constructor, Clone)]
pub struct InitialDraw {}

impl game_start::Action for InitialDraw {
    fn action(&self, game: &mut Game, _card: usize, data: game_start::EventData) -> Option<game_start::EventData> {
        for player in game.environment.players.iter_mut() {
            for _ in 0..4 {
                player.draw_card(); //TODO: use draw event
            }
        }
        Some(data)
    }
}

#[derive(Constructor, Clone)]
pub struct UseMana {}

impl mana_use::Action for UseMana {
    fn action(&self, game: &mut Game, _card: usize, data: mana_use::EventData) -> Option<mana_use::EventData> {
        game.environment.card(game.environment.players[data.player].hero).unwrap().character().unwrap().hero().unwrap().mana -= data.amount;
        Some(data)
    }
}
