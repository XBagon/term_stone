use std::fmt;

#[derive(Clone, Debug)]
pub struct Spell {}

impl fmt::Display for Spell {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "[Spell]")
    }
}