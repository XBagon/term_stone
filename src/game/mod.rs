pub mod deck;
pub mod events;
pub mod player;

use self::{
    deck::Deck,
    events::{game_start, play, EventManager},
    player::Player,
};
use crate::cards::{game_modes::default_game_mode, Card};
use itertools::Itertools;
use rand::seq::SliceRandom;
use std::iter;
#[derive(Debug)]
pub struct Game {
    pub event_manager: EventManager,
    pub environment: Environment,
}

#[derive(Debug)]
pub struct Environment {
    pub game_mode: usize,
    pub players: Vec<Player>,
    pub cards: Vec<Card>,
    pub current_player: usize,
    pub turn: usize,
}

impl Environment {
    pub fn card(&mut self, card_id: usize) -> Option<&mut Card> {
        self.cards.get_mut(card_id)
    }

    pub fn player(&mut self, player_id: usize) -> Option<&mut Player> {
        self.players.get_mut(player_id)
    }

    pub fn card_iter<'a>(&'a self, slice: &'a [usize]) -> impl Iterator<Item = (usize, &'a Card)> + 'a {
        let cards = &self.cards;
        slice.iter().map(move |&id| (id, &cards[id]))
    }

    /*
    Can't work without unsafe as an Iterator, because Rust is unable to guarantee that I don't have 2 &mut to the same card.

    pub fn card_iter_mut<'a>(&'a mut self, slice: &'a [usize]) -> impl Iterator<Item=(usize,&mut Card)> + 'a {
        let cards = &mut self.cards;
        slice.iter().map(|&id| (id, &cards[id]))
    }
    */
}

impl Game {
    pub fn new(player_decks: Vec<Deck>, starting_player: usize) -> Self {
        let mut game = Game {
            event_manager: EventManager::new(),
            environment: Environment {
                game_mode: 0,
                players: Vec::new(),
                cards: Vec::new(),
                current_player: starting_player,
                turn: 0,
            },
        };

        let mut rng = rand::thread_rng();

        game.environment.game_mode = game.initialize_card(default_game_mode::new());

        let mut decks = vec![Vec::new(); player_decks.len()];
        for (player_id, player_deck) in player_decks.iter().enumerate().cycle().skip(starting_player).take(player_decks.len()) {
            let card_id = game.initialize_card(player_deck.hero.clone());
            game.environment.players.push(Player::new(card_id));
            decks[player_id] = player_deck.cards.clone();
            decks[player_id].shuffle(&mut rng);
        }

        for (player_id, card) in decks.iter().enumerate().map(|(i, d)| iter::repeat(i).zip(d)).kmerge_by(|_, _| true) {
            let card_id = game.initialize_card(card.clone());
            game.environment.players[player_id].deck.cards.push(card_id);
        }

        game
    }

    fn initialize_card(&mut self, card: Card) -> usize {
        let id = self.environment.cards.len();
        (card.init)(&mut self.event_manager, id);
        self.environment.cards.push(card);
        id
    }

    pub fn start(&mut self) {
        self.trigger_game_start(game_start::EventData::new());
    }

    pub fn play_card(&mut self, player: usize, card: usize, target: Option<usize>, position: Option<usize>) {
        let position = if let Some(position) = position {
            position
        } else {
            self.environment.players[player].board.cards.len()
        };
        self.trigger_play(play::EventData::new(player, card, target, position));
    }
}
