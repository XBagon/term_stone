#[derive(Clone, Debug, Default)]
pub struct Player {
    pub hero: usize,
    pub hand: Hand,
    pub deck: Deck,
    pub board: Board,
    pub graveyard: Graveyard,
}

//TODO: Think about having a wrapper type CardId instead of usize

impl Player {
    pub fn new(hero: usize) -> Player {
        Player {
            hero,
            hand: Hand::default(),
            deck: Deck::default(),
            board: Board::default(),
            graveyard: Graveyard::default(),
        }
    }

    pub fn draw_card(&mut self) -> bool {
        if let Some(card) = self.deck.cards.pop() {
            self.hand.cards.push(card);
            true
        } else {
            false
        }
    }

    pub fn play_at_position(&mut self, card: usize, position: usize) {
        self.board.cards.insert(position, self.hand.try_remove(card).unwrap());
    }
}

trait CardRemove {
    fn try_remove(&mut self, card: usize) -> Option<usize>;
}

#[derive(Clone, Debug, Default)]
pub struct Hand {
    pub cards: Vec<usize>,
}

impl CardRemove for Hand {
    fn try_remove(&mut self, card: usize) -> Option<usize> {
        if let Some(e) = self.cards.iter().position(|&e| e == card) {
            Some(self.cards.remove(e))
        } else {
            None
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Deck {
    pub cards: Vec<usize>,
}

#[derive(Clone, Debug, Default)]
pub struct Board {
    pub cards: Vec<usize>,
}

impl Board {}

#[derive(Clone, Debug, Default)]
pub struct Graveyard {
    pub cards: Vec<usize>,
}
