use crate::cards::Card;

#[derive(Debug)]
pub struct Deck {
    pub hero: Card,
    pub cards: Vec<Card>,
}

impl Deck {
    pub fn new(hero: Card, deck: Vec<Card>) -> Self {
        Deck {
            hero,
            cards: deck,
        }
    }
}
