//TODO: use concat_idents when stable: concat_idents!(trigger_, $event_name)
macro_rules! events {
    ( $( $event:ident, $trigger_event:ident, $before_event:ident, $on_event:ident, $after_event:ident ($($param_name:ident : $param_type:ty),*) ;)* ) => {
        use super::Game;
        use objekt::clone_box;
        #[derive(Debug)]
        pub struct EventManager {
            $(
                pub $before_event: Vec<$event::BeforeSubscription>,
                pub $on_event: Vec<$event::OnSubscription>,
                pub $after_event: Vec<$event::AfterSubscription>
            ),*
        }

        impl EventManager {
            pub fn new() -> Self {
                EventManager {
                    $(
                        $before_event: Vec::new(),
                        $on_event: Vec::new(),
                        $after_event: Vec::new()
                    ),*
                }
            }
        }

        impl Game {
            pub fn execute<T: Subscription>(&mut self, sub: T, data: T::EventData) -> Option<T::EventData> {
                sub.execute(self, data)
            }
        }

        pub trait Subscription : Clone {
            type EventData;

            fn execute(&self, game: &mut Game, data: <Self as Subscription>::EventData) -> Option<<Self as Subscription>::EventData>;

            fn subscriber(&self) -> usize;

            fn description(&self) -> String;
        }

        $(
            pub mod $event {
                use super::*;
                use std::fmt;

                #[derive(Clone,Constructor)]
                pub struct EventData {
                    $(pub $param_name : $param_type),*
                }

                pub trait Action : objekt::Clone {
                    fn action(&self, game: &mut Game, card: usize, data: EventData) -> Option<EventData>;
                }


                #[derive(Constructor)]
                pub struct BeforeSubscription {
                    pub subscriber: usize,
                    pub action: Box<dyn Action>,
                }

                impl Subscription for BeforeSubscription {
                    type EventData = EventData;

                    fn execute(&self, game: &mut Game, data: EventData) -> Option<EventData> {
                        self.action.action(game, self.subscriber, data)
                    }

                    fn subscriber(&self) -> usize {
                        self.subscriber
                    }

                    fn description(&self) -> String {
                        String::new()
                    }
                }

                impl Clone for BeforeSubscription {
                    fn clone(&self) -> Self {
                        BeforeSubscription {
                            subscriber: self.subscriber.clone(),
                            action: clone_box(&*self.action),
                        }
                    }
                }

                impl fmt::Debug for BeforeSubscription {
                    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
                        write!(f, concat!(stringify!($event),"BeforePlaySubscription {{ subscriber: {:?}, description: {:?} }}"), self.subscriber, self.description())
                    }
                }


                #[derive(Constructor)]
                pub struct OnSubscription {
                    pub subscriber: usize,
                    pub action: Box<dyn Action>,
                }

                impl Subscription for OnSubscription {
                    type EventData = EventData;

                    fn execute(&self, game: &mut Game, data: EventData) ->  Option<EventData> {
                        self.action.action(game, self.subscriber, data)
                    }

                    fn subscriber(&self) -> usize {
                        self.subscriber
                    }

                    fn description(&self) -> String {
                        String::new()
                    }
                }

                impl Clone for OnSubscription {
                    fn clone(&self) -> Self {
                        OnSubscription {
                            subscriber: self.subscriber.clone(),
                            action: clone_box(&*self.action),
                        }
                    }
                }

                impl fmt::Debug for OnSubscription {
                    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                        write!(f, concat!(stringify!($event),"OnPlaySubscription {{ subscriber: {:?}, description: {:?} }}"), self.subscriber, self.description())
                    }
                }


                #[derive(Constructor)]
                pub struct AfterSubscription {
                    pub subscriber: usize,
                    pub action: Box<Action>,
                }

                impl Subscription for AfterSubscription {
                    type EventData = EventData;

                    fn execute(&self, game: &mut Game, data: EventData) -> Option<EventData> {
                        self.action.action(game, self.subscriber, data)
                    }

                    fn subscriber(&self) -> usize {
                        self.subscriber
                    }

                    fn description(&self) -> String {
                        String::new()
                    }
                }

                impl Clone for AfterSubscription {
                    fn clone(&self) -> Self {
                        AfterSubscription {
                            subscriber: self.subscriber.clone(),
                            action: clone_box(&*self.action),
                        }
                    }
                }

                impl fmt::Debug for AfterSubscription {
                    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                        write!(f, concat!(stringify!($event),"::AfterPlaySubscription {{ subscriber: {:?}, description: {:?} }}"), self.subscriber, self.description())
                    }
                }

                impl Game {
                    pub fn $trigger_event(&mut self, data: EventData) -> Option<EventData> {
                        let mut current_value = Some(data);
                        for sub in self.event_manager.$before_event.clone().into_iter() {
                            if let Some(data) = current_value {
                                current_value = self.execute(sub.clone(), data);
                            } else {
                                return None;
                            }
                        }
                        for sub in self.event_manager.$on_event.clone().into_iter() {
                            if let Some(data) = current_value {
                                current_value = self.execute(sub.clone(), data);
                            } else {
                                return None;
                            }
                        }
                        for sub in self.event_manager.$after_event.clone().into_iter() {
                            if let Some(data) = current_value {
                                current_value = self.execute(sub.clone(), data);
                            } else {
                                return None;
                            }
                        }
                        current_value
                    }
                }

            }
        )*
    };
}

events! {
    game_start, trigger_game_start, before_game_start, on_game_start, after_game_start ();
    card_draw, trigger_card_draw, before_card_draw, on_card_draw, after_card_draw (player: usize, drawing_card: usize, drawn_cards: Vec<usize>);
    play, trigger_play, before_play, on_play, after_play (player: usize, playing_card: usize, target: Option<usize>, position: usize);
    mana_use, trigger_mana_use, before_mana_use, on_mana_use, after_mana_use (player: usize, mana_using_card: usize, amount: i32);
    mana_cost_check, trigger_mana_cost_check, before_mana_cost_check, on_mana_cost_check, after_mana_cost_check (player: usize, mana_cost_checking_card: usize, cost: i32);
    cost_pay, trigger_cost_pay, before_cost_pay, on_cost_pay, after_cost_pay (player: usize, paying_card: usize, cost: i32);
}
