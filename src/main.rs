#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate derive_more;
#[macro_use]
extern crate derivative;
#[macro_use]
extern crate clap;

use crate::{
    cards::characters::{heroes::jaina_proudmoore, minions::elven_archer},
    game::{deck::Deck, Game},
};

mod cards;
mod game;
mod interface;

fn main() {
    let players = vec![Deck::new(jaina_proudmoore::new(), vec![elven_archer::new(); 30]), Deck::new(jaina_proudmoore::new(), vec![elven_archer::new(); 30])];
    let mut game = Game::new(players, 0);
    game.start();

    interface::start(&mut game, 0);
/*
    let card_to_play = game.environment.players[0].hand.cards[0];
    let target = game.environment.players[1].hero;
    game.environment.card(game.environment.players[0].hero).unwrap().character().unwrap().hero().unwrap().mana = 6;
    game.play_card(0, card_to_play, Some(target), 0);
    eprintln!("game = {:#?}", game);
*/

}